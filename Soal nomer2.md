```sql
CREATE TABLE `User` (
  `id_user` int  PRIMARY KEY ,
  `name` varchar(255),
  `phone_number` varchar(20),
  `email` varchar(255),
  `katasandi` varchar(255),
  `balance` decimal,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Driver` (
  `id_driver` int  PRIMARY KEY ,
  `name` varchar(255),
  `phone_number` varchar(20),
  `email` varchar(255),
  `katasandi` varchar(255),
  `rating` decimal,
  `car_type` varchar(255),
  `status` varchar(20),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Order` (
  `id_order` int  PRIMARY KEY,
  `id_promo` int,
  `id_ride` int,
  `id_user` int,
  `id_driver` int,
  `order_time` datetime,
  `pickup_location` varchar(255),
  `destination_location` varchar(255),
  `status` varchar(20),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Payment` (
  `id_payment` int  PRIMARY KEY ,
  `id_order` int,
  `amount` decimal,
  `payment_time` datetime,
  `payment_method` varchar(20),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Walet` (
  `id_wallet` int PRIMARY KEY ,
  `id_user` int,
  `balance` decimal,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Rating` (
  `id_rating` int  PRIMARY KEY ,
  `id_user` int,
  `id_driver` int,
  `rating` int,
  `comment` varchar(255),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Location` (
  `id_location` int  PRIMARY KEY ,
  `name` varchar(255),
  `latitude` decimal,
  `longitude` decimal,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Vehicle` (
  `id_vehicle` int  PRIMARY KEY,
  `id_driver` int,
  `id_ride` int,
  `type` varchar(255),
  `make` varchar(255),
  `model` varchar(255),
  `plate_number` varchar(20),
  `year` int,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Promo` (
  `id_promo` int PRIMARY KEY ,
  `id_order` int,
  `code` varchar(20),
  `description` varchar(255),
  `discount` decimal,
  `valid_from` datetime,
  `valid_until` datetime,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `Ride` (
  `id_ride` int  PRIMARY KEY,
  `id_order` int,
  `id_user` int,
  `id_driver` int,
  `id_vehicle` int,
  `id_location_pickup` int,
  `id_location_destination` int,
  `start_time` datetime,
  `end_time` datetime,
  `status` varchar(20),
  `created_at` datetime,
  `updated_at` datetime
);

ALTER TABLE `Walet` ADD FOREIGN KEY (`id_user`) REFERENCES `User` (`id_user`);

ALTER TABLE `Order` ADD FOREIGN KEY (`id_user`) REFERENCES `User` (`id_user`);

ALTER TABLE `Vehicle` ADD FOREIGN KEY (`id_driver`) REFERENCES `Driver` (`id_driver`);

ALTER TABLE `Ride` ADD FOREIGN KEY (`id_ride`) REFERENCES `Order` (`id_order`);

ALTER TABLE `Payment` ADD FOREIGN KEY (`id_order`) REFERENCES `Order` (`id_order`);

ALTER TABLE `Rating` ADD FOREIGN KEY (`rating`) REFERENCES `Driver` (`id_driver`);

ALTER TABLE `Rating` ADD FOREIGN KEY (`id_user`) REFERENCES `User` (`id_user`);

ALTER TABLE `Ride` ADD FOREIGN KEY (`id_user`) REFERENCES `Driver` (`id_driver`);

ALTER TABLE `Ride` ADD FOREIGN KEY (`id_user`) REFERENCES `User` (`id_user`);

ALTER TABLE `Vehicle` ADD FOREIGN KEY (`id_ride`) REFERENCES `Ride` (`id_ride`);

ALTER TABLE `Order` ADD FOREIGN KEY (`id_order`) REFERENCES `Promo` (`id_order`);

ALTER TABLE `Order` ADD FOREIGN KEY (`id_promo`) REFERENCES `Promo` (`id_promo`);

ALTER TABLE `Ride` ADD FOREIGN KEY (`id_location_pickup`) REFERENCES `Location` (`id_location`);

ALTER TABLE `Ride` ADD FOREIGN KEY (`id_location_destination`) REFERENCES `Location` (`id_location`);
