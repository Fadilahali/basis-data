## Printah Dql
## 1. Berapa banyak pengguna yang belum pernah menggunakan kode promo?

```sql
SELECT COUNT(*) as total_user
FROM (
    SELECT DISTINCT id_user
    FROM `Order`
    WHERE id_promo IS NULL
) as no_promo_user;
```

## 2. Berapa total saldo saat ini di semua dompet digital?
```sql
SELECT SUM(balance) as total_balance
FROM Walet;
```
 
## 3. berapa jumalah transaksi setiap user ?
```sql
SELECT name, COUNT(*) as jumlah
FROM User 
INNER JOIN 'payment'ON id_user = id_user
GROUP BY name
ORDER BY jumlah DESC;
```
## 4. berapa jumlah order dilakukan semua user ?
```sql
SELECT COUNT(*) AS jumlah_order
FROM 'Order';
```
## 5. berapa nilai rata rata rating yang diberikan user kepada driver ?
```sql
 SELECT AVG(rating) as average_rating FROM Driver GROUP BY id_driver;
 ```
 
## 6. saldo terbanyak yang di miliki oleh seorang user ?
```sql
SELECT MAX(balance) AS balance_terbanyak
FROM User
WHERE id_user = [ID_USER]
```
