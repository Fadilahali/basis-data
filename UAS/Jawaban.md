# 1. Menampilkan use case table untuk produk digitalnya
Use Case | bisa | Nilai Priorita|
---|---|---
user | dapat mendaftar Akun |100
user | login menggunkan akun yang terdaftar | 100
user | dapat memilih  menu yang di inginkan | 100
user | dapat memilih menu goride | 100
user | memilih driver yang tersedia di lokasi terdekat | 80
user | bisa menuntukan titik jemput  | 80
user | bisa memilih kendaraan yang tersedia ``| 90
user | user bisa order dan tunggu driver menjemput | 100
user | user dapat melihat saldo gopay  | 80
user | user dapat menambahkan vocher diskon | 40
user | bisa memilih menu Gofood | 100
user | user memasukan lokasi  | 80
user | memilih  item yang di inginkan | 80
user | user memasukan item yang di pilih ke keranjang | 80
user | user meng cekout item yang di pilih | 80
user | driver siap mengantarkan item | 80
user | user dapat memilih Gocar  |80 
user | user dapat memilih lokasi jemput Gocar | 80
user | user dapat Memili Gocar yang Ready di sekitar | 80
user | user bisa toup up Gopay |80 
user | user dapat bayar mengunakan Gopay | 80
user | user dapat memilih menu Gosend|80
user | user memilih pengiriman ke dalam atau luar kota |80
user | user dapat measuka data diri pengirim |70
user | user dapat memilih menu Gomart | 80
user | user dapat memilih barang atau makanan yang akan di beli | 70
user | user melihat pesanan yang sudah di lakukan di Gomart |70
user | user dapat melihat promo promo Gojek | 80
user | user dapat melihat promo yang di miliki | 80
user | user dapat menggunakan Promo yang tersedia |70
user | user dapat melakukan chat dengan Driver | 80
user | user dapat memilih Gotagihan | 80
user | di Go tagihan user bisa mau bayar tagihan apa | 60
user | dapat Isi ulang segala prabayar | 60
user | dapat memesan kreta di Gotransit |70
user | dapat membeli segala jenis Obat di Gomed | 70
user | dapat bergabung dengan Goclub | 60
user | dapat dapat memesan jasa angkut barang di GoBox|70
user | dapat membeli segala tiket Di Gotix | 70
user | bisa Melihat perjalanan | 80
user | dapat nonton tayangan eksklusif dari Goplay | 60
menejemen | dapat melihat banyaknya pengguna | 40
menejemen | dapat memasukan Code Promo | 40
menejemen | dapat melihat berapa banyak user mengunakan code promo | 40
direksi | dapat melihat pertumbuhan pengguna | 40
direksi | dapat melihat kepuasan pengguna | 40


# 2. Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya:
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/webse.gif)
#    Conected to Data Base
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/conect.gif)
# 3. Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya:
# 4. Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan
Regex 
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/regex.gif)
Substring
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/UAS/substring.gif)
# 5. Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya
-Order yang dilakukan setiap user perbulannya
```sql
SELECT id_user, TO_CHAR(start_time, 'YYYY-MM') AS ride_month, COUNT(*) AS total_ride_orders
FROM Ride
GROUP BY id_user, ride_month;
```
# 6. Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya
~~~sql
SELECT t.id_transaction, t.transaction_time, u.name AS user_name, d.name AS driver_name, r.id_ride, t.amount
FROM transaction t
INNER JOIN users u ON t.id_user = u.id_user
INNER JOIN driver d ON t.id_driver = d.id_driver
INNER JOIN ride_order r ON t.id_order = r.id_order;
~~~
# 7. Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya
Procedure
~~~sql
---Procrdure
CREATE OR REPLACE PROCEDURE UpdateOrderStatus(orderId INT, newStatus VARCHAR(20))
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE orders
  SET status = newStatus
  WHERE id = orderId;
  COMMIT; 
END;
$$;
~~~
Function
~~~sql
--- Fuctions
CREATE OR REPLACE FUNCTION CalculateOrderTotal(orderId INT)
RETURNS DECIMAL(10, 2) AS $$
DECLARE
  total DECIMAL(10, 2);
BEGIN
  SELECT SUM(price * quantity) INTO total
  FROM order_items
  WHERE order_id = orderId;
  
  RETURN total;
END;
$$ LANGUAGE plpgsql;
~~~
Trigger
~~~sql
--- "transaction" t2 
SELECT t.id_transaction, t.transaction_time, u.name AS user_name, d.name AS driver_name, r.id_ride, t.amount
FROM transaction t
INNER JOIN users u ON t.id_user = u.id_user
INNER JOIN driver d ON t.id_driver = d.id_driver
INNER JOIN ride_order r ON t.id_order = r.id_order;

---Trigers
CREATE OR REPLACE FUNCTION UpdateUserBalance()
RETURNS TRIGGER AS $$
DECLARE
  userId INT;
  amount DECIMAL(10, 2);
BEGIN
  SELECT user_id, transaction_amount INTO userId, amount
  FROM transactions
  WHERE id = NEW.id;

  UPDATE users
  SET balance = balance - amount
  WHERE id = userId;

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
~~~
# 8. Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya
# 9. Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya
![](https://gitlab.com/Fadilahali/basis-data/-/raw/main/ii.gif)
# 10. Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
# 11. BONUS !!! Mendemonstrasikan UI untuk CRUDnya
saya membuat crud sederhana untuk manambahkan menghapus dan update table users
![](https://gitlab.com/tubes-pbo1/tubes/-/raw/main/interfCW.gif)


