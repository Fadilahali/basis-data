## use case
user|bisa|Priorty|
----|----|------|
Mangement|bisa melihat seluruh Yang Terdaftar|100
management|bisa menghapus user yang Sudah Tidap aktif    |100
manegemnt|bisa melihat semua promo yang di sediakan | 100
management|bisa melihat berapa banyak yang Order | 100
managemnt |Bisa Menambahkan Kode Promo lagi |100


## sesuai data CRUD

## Menambahkan Data Dumy Ke User 
```sql
INSERT INTO `User` (`id_user`, `name`, `phone_number`, `email`, `katasandi`, `balance`, `created_at`, `updated_at`)
VALUES
(1, 'John Doe', '081234567890', 'johndoe@example.com', 'password123', 500000, '2022-01-01 00:00:00', '2022-01-01 00:00:00'),
(2, 'Jane Doe', '082345678901', 'janedoe@example.com', 'password456', 700000, '2022-01-02 00:00:00', '2022-01-02 00:00:00'),
(3, 'Bob Smith', '083456789012', 'bobsmith@example.com', 'password789', 900000, '2022-01-03 00:00:00', '2022-01-03 00:00:00'),
(4, 'Alice Johnson', '084567890123', 'alicejohnson@example.com', 'passwordabc', 600000, '2022-01-04 00:00:00', '2022-01-04 00:00:00'),
(5, 'Mike Brown', '085678901234', 'mikebrown@example.com', 'passworddef', 800000, '2022-01-05 00:00:00', '2022-01-05 00:00:00'),
(6, 'Emily Davis', '086789012345', 'emilydavis@example.com', 'passwordghi', 400000, '2022-01-06 00:00:00', '2022-01-06 00:00:00'),
(7, 'David Lee', '087890123456', 'davidlee@example.com', 'passwordjkl', 300000, '2022-01-07 00:00:00', '2022-01-07 00:00:00'),
(8, 'Karen Miller', '088901234567', 'karenmiller@example.com', 'passwordmno', 1000000, '2022-01-08 00:00:00', '2022-01-08 00:00:00'),
(9, 'Tom Brown', '089012345678', 'tombrown@example.com', 'passwordpqr', 1200000, '2022-01-09 00:00:00', '2022-01-09 00:00:00'),
(10, 'Sara Wilson', '081112345678', 'sarawilson@example.com', 'passwordstu', 500000, '2022-01-10 00:00:00', '2022-01-10 00:00:00'),
(11, 'Max Martin', '081212345679', 'maxmartin@example.com', 'passwordvwx', 800000, '2022-01-11 00:00:00', '2022-01-11 00:00:00'),
(12, 'Lisa Johnson', '081312345680', 'lisajohnson@example.com', 'passwordyz1', 600000, '2022-01-12 00:00:00', '2022-01-12 00:00:00'),
(13, 'Eric Davis', '081412345681', 'ericdavis@example.com', 'password234',300000,'2022-01-12 00:00:00', '2022-01-12 00:00:00');
```

## Menampilkan Semua User yang Terdaftar
```sql
SELECT*FROM User;
```
 
## Mengubah data Yang tidat sesuai
```sql
UPDATE User
set name = "udin"
WHERE id_user = 1;
```
 
## Untuk Mengahpus User yang sudah Tidak aktif
```sql
DELETE FROM User WHERE name = "udin";
```
 
 ## Berapa Banyak User yang Toup di bawah 70000

SELECT*FROM User WHERE balance <= 70000.0;

## Melihat berapa banyak yang Melakukan Order
```sql
SELECT*FROM Order;
